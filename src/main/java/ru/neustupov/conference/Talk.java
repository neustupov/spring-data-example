package ru.neustupov.conference;

import java.time.LocalDateTime;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Talk {

  @Id
  @GeneratedValue
  private Long talkId;

  private LocalDateTime when;

  private String title;

  public Talk() {
  }

  public Talk(String title, LocalDateTime date) {
    this.when = date;
    this.title = title;
  }

  public LocalDateTime getWhen() {
    return when;
  }

  @Override
  public String toString() {
    return "Talk{" +
        "talkId=" + talkId +
        ", when=" + when +
        ", title='" + title + '\'' +
        '}';
  }
}
