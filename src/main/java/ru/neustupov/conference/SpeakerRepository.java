package ru.neustupov.conference;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SpeakerRepository extends PagingAndSortingRepository<Speaker, Long> {

  Speaker findByName(String name);

  List<Speaker> findПожалуйстаByNameLike(String name);

  List<Speaker> findByNameEndingWith(String suffix);

  List<Speaker> findByTalksTitleLikeIgnoreCase(String partOfTalkTitle);

  List<Speaker> findByTalksWhenBetween(LocalDateTime from, LocalDateTime to);
}
