package ru.neustupov.conference;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
@Transactional
public class AppTest {

  private static final Logger LOG = LoggerFactory.getLogger(AppTest.class);

  @Autowired
  private SpeakerRepository speakerRepository;

  @Before
  @Rollback(false)
  public void setUp() {

    Speaker evgeny = new Speaker("mr Evgeny");
    Speaker jeka = new Speaker("mr Jeka");

    jeka.addTalk(new Talk("The Internals of Spring", LocalDateTime.parse("2018-12-03T10:00:00")));
    jeka.addTalk(new Talk("The Spring modules", LocalDateTime.parse("2018-12-06T11:00:00")));

    Speaker nikolay = new Speaker("mr Nikolay");
    nikolay.addTalk(new Talk("JEE 7", LocalDateTime.parse("2018-11-03T14:30:00")));

    Speaker baruh = new Speaker("mr Baruh");
    baruh.addTalk(new Talk("Groovy", LocalDateTime.parse("2019-12-03T12:00:00")));
    baruh.addTalk(new Talk("Making Spring Groovy", LocalDateTime.parse("2018-12-15T10:45:00")));

    speakerRepository.save(evgeny);
    speakerRepository.save(jeka);
    speakerRepository.save(nikolay);
    speakerRepository.save(baruh);
  }

  @Test
  public void testCount() {
    LOG.info("+++++++++++++++++++++++++ Number of speakers +++++++++++++++++++++++++");
    LOG.info("Speakers: " + speakerRepository.count());
    LOG.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
  }

  @Test
  public void testFindAll() {
    LOG.info("+++++++++++++++++++++++++ All speakers +++++++++++++++++++++++++++++++");
    Iterable allSpeakers = speakerRepository.findAll(new Sort(Direction.DESC, "name"));
    for (Object speaker : allSpeakers) {
      LOG.info(speaker.toString());
    }
    LOG.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
  }

  @Test
  public void testFindByName() {
    LOG.info("++++++++++++++++++++++++ All talks baruh +++++++++++++++++++++++++++++");
    Speaker speaker = speakerRepository.findByName("mr Baruh");
    Set<Talk> talks = speaker.getTalks();
    for (Talk talk : talks) {
      LOG.info("talk= " + talk);
    }
    LOG.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
  }

  @Test
  public void testFindByNameLike() {
    LOG.info("++++++++++++++++++++++++ All jekas +++++++++++++++++++++++++++++");
    List<Speaker> speakers = speakerRepository.findПожалуйстаByNameLike("%Jeka%");
    speakers.forEach(s -> LOG.info(s.toString()));
    LOG.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
  }

  @Test
  public void testFindByNameSuffix() {
    LOG.info(
        "++++++++++++++++++++++++ All speaker whose name ends of y +++++++++++++++++++++++++++++");
    List<Speaker> speakers = speakerRepository.findByNameEndingWith("y");
    speakers.forEach(s -> LOG.info(s.toString()));
    LOG.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
  }

  @Test
  public void testFindByTalkTitle() {
    LOG.info(
        "++++++++++++++++++++++++ talks with spring     +++++++++++++++++++++++++++++");
    List<Speaker> speakers = speakerRepository.findByTalksTitleLikeIgnoreCase("%spring%");
    speakers.forEach(s -> LOG.info(s.toString()));
    LOG.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
  }

  @Test
  public void testFindByDate() {
    LOG.info("++++++++++++++++++++++++ talks with spring     +++++++++++++++++++++++++++++");
    LocalDateTime from = LocalDateTime.parse("2018-12-02T10:00:00");
    LocalDateTime to = LocalDateTime.parse("2018-12-30T10:00:00");
    List<Speaker> speakers = speakerRepository.findByTalksWhenBetween(from, to);
    speakers.stream().map(Speaker::getTalks).flatMap(Collection::stream)
        .filter(t -> isBetween(from, to, t)).collect(Collectors.toList())
        .forEach(t -> LOG.info(t.toString()));
    LOG.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
  }

  public boolean isBetween(LocalDateTime from, LocalDateTime to, Talk talk) {
    return talk.getWhen().isAfter(from) && talk.getWhen().isBefore(to);
  }

  @After
  public void clean() {
    speakerRepository.deleteAll();
  }
}
